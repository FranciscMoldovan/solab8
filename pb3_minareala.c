/*
Scrieti un program care genereaza N thread-uri sau N procese, 
fiecare din acestea afisând ı̂ntr-o buclă infinită propriul id, 
apoi asteaptă un timp aleatoriu. Sincronizati cele N 
thread-uri sau procese, astfel id-urile să se afiseze alternativ
(de exemplu daca avem 3 procese se va afisa 1 2 3 1 2 3 1 2 ...).
Indicatie: Pentru a va asigura ca textul afisat cu printf ajunge
pe ecran la momentul respectiv, după apelul de printf apelati 
fflush(stdout).
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <semaphore.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>

int turn = 0;

typedef struct
{
    sem_t *semas;
    int   id;
    int   numThreads;
}ThreadDataT;

void *thread_func(void *args)
{

}

int main(int argc, char **argv)
{
    srand(time(NULL));

    pthread_t   *threads;
    sem_t       *semaphores;
    ThreadDataT *threadData;

    int i, num_threads = 0;

    if(2 != argc)
    {
        printf("USAGE: %s NUM_THREADS\n", argv[0]);
        exit(-1);
    }

    sscanf(argv[1], "%d", &num_threads);

    threads     =   (pthread_t*)malloc(num_threads*sizeof(pthread_t));
    semaphores  =       (sem_t*)malloc(num_threads*sizeof(sem_t));
    threadData  = (ThreadDataT*)malloc(num_threads*sizeof(ThreadDataT));

    // 1) Init the semas with 0
    for(i=0; i<num_threads; i++)
    {
        sem_init(&semaphores[i], 0, 0);
    }

    // 2) Unlock thread #1
    sem_post(&semaphores[0]);

    for(i=0; i<num_threads; i++)
    {
        threadData[i].id = i; // each th gets its "ID"
        threadData[i].semas = semaphores; // and sema array
        threadData[i].numThreads = num_threads; // feed this, too
    }

    for(i=0; i<num_threads; i++)
    {
        pthread_create(&threads[i], NULL, thread_func, &threadData[i]);
    }

    for(i=0; i<num_threads; i++)
    {
        pthread_join(threads[i], NULL);
    }



free(threads);
free(threadData);
free(semaphores);

return 0;
}