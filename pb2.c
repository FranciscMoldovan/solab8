#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <semaphore.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>

#define NUM_THREADS 16

int nrThreads = 0;

void *limited_area(void *unused)
{
    sem_t *sema = (sem_t*)unused;

sem_wait(sema);
    nrThreads++;
    usleep(500);
    printf("The number of threads in the limited area is %d\n", nrThreads);
    nrThreads--;
sem_post(sema);
    return NULL;
}

int main(int argc, char**argv)
{
    int i;
    int maxThCnt=0;
    pthread_t threads[NUM_THREADS];
    sem_t mySema;

    if(2 != argc)
    {
        printf("USAGE: %s MAX_THREADS_CRITICAL_AREA\n", argv[0]);
        exit(-1);
    }

    sscanf(argv[1], "%d", &maxThCnt);

    if(sem_init(&mySema, 0, maxThCnt)!=0)
    {
        perror("SEMA error!");
        return -1;
    }

    for(i=0; i<NUM_THREADS;i++)
    {
        pthread_create(&threads[i], NULL, limited_area, &mySema);        
    }

    for(i=0; i<NUM_THREADS; i++)
    {
        pthread_join(threads[i], NULL);
    }

    sem_close(&mySema);

    return 0;
}











