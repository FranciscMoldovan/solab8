#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <semaphore.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>

#define N 4
#define M 1000

long count = 0;

void *thread_function(void* unused)
{
    sem_t* ptrSem = (sem_t*)unused;

    int  i;
    long aux; 

sem_wait(ptrSem);
    for(i=0; i<M; i++)
    {
        aux = count;
        aux ++;
        usleep(random() % 10);
        count = aux;
    }
sem_post(ptrSem);



    return NULL;
}

int main(void)
{
    pthread_t threads[N];
    sem_t incSem;
    int i;
    
    if(sem_init(&incSem, 0, 1)!=0)
    {
        perror("SEMA error");
        return -1;
    }

    for(i=0; i<N; i++)
    {
        pthread_create(&threads[i], NULL, thread_function, &incSem);
    }

    for(i=0; i<N; i++)
    {
        pthread_join(threads[i], NULL);
    }

    printf("count's value is: %ld\n", count);

    sem_close(&incSem);

    return 0;
}