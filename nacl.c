#include <stdio.h>
#include <semaphore.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>

sem_t sem_na, sem_cl_na;
sem_t sem_cl, sem_na_cl;
sem_t sem_print;

pthread_t thread_na, thread_cl;

void *func_na(void *unused)
{
    sem_wait(&sem_na); // 1) Sodium waiting for Clorine
        thread_na = pthread_self(); // 2) thread identifies self
        sem_post(&sem_cl_na); // 3) cl_na has been formed
            sem_wait(&sem_na_cl); // 4) wait for opposite
                sem_wait(&sem_print);
                printf("I am Na(%llu) and I combined with Cl(%llu)\n", (unsigned long long)thread_na, \
                        (unsigned long long)thread_cl);
                fflush(stdout);
                sem_post(&sem_print);
        sem_post(&sem_cl_na);
            sem_wait(&sem_na_cl);
    sem_post(&sem_na);
    return NULL;    

}

void *func_cl(void *unused)
{
    sem_wait(&sem_cl); // 1) Clorine waiting for Sodium
        thread_cl = pthread_self(); // 2) thread identifies self
        sem_post(&sem_na_cl); // 3) na_cl has been formed
            sem_wait(&sem_cl_na); // 4) wait for oppiosite
                sem_wait(&sem_print);
                printf("I am Cl(%llu) and I combined with Na(%llu)\n", (unsigned long long)thread_cl, \
                        (unsigned long long)thread_na);
                fflush(stdout);
                sem_post(&sem_print);
        sem_post(&sem_na_cl);
            sem_wait(&sem_cl_na);
    sem_post(&sem_cl);
    return NULL;
}

int main()
{
    pthread_t thread;

    sem_init(&sem_na, 0, 1);
    sem_init(&sem_cl, 0, 1);
    sem_init(&sem_cl_na, 0, 1);
    sem_init(&sem_na_cl, 0, 1);

    sem_init(&sem_print, 0, 1);

    sem_wait(&sem_cl_na); // Wait for NaCl formation
    sem_wait(&sem_na_cl); // Wait for ClNa formation

    for(;;)
    {
        sleep(random() % 3);
        if(random() % 2)
        {
            pthread_create(&thread, NULL, func_na, NULL);
        }
        else
        {
            pthread_create(&thread, NULL, func_cl, NULL); 
        }
    }



    return 0;
}